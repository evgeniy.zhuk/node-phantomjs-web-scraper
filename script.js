'use strict';
const async = require('async');
const request = require('request');
const cheerio = require('cheerio');
const phantomjs = require('phantomjs');
const casper = require('casperjs/modules/casper.js').create();


module.exports = function parse() {
    var uri = "http://madrobots.ru";
    request(uri, function (err, res, body) {
        if (err) {
            throw err;
        } else {
            var $ = cheerio.load(body);
            var category = $(".header .category");
            var itemSections = $('.header .category .category__item');
            var itemList = [];

            itemSections.each(function () {
                itemList.push({
                    sectionLink: $('.category__link', this).attr('href'),
                    sectionTitle: $('.category__title', this).text()
                })
            });

            //noinspection JSAnnotator
            function parsePage(sectionPage, paginatorPage, category, callback) {
                request(uri + sectionPage + '?PAGEN_1=' + paginatorPage, function (err, res, body) {
                    if (err) {
                        throw err;
                    } else {
                        var $ = cheerio.load(body);
                        var items = [];
                        $(".catalog-grid__item").each(function (inc) {
                            items.push({
                                id: inc,
                                title: $('.good__name', this).text(),
                                category: category,
                                link: $('a', this).attr('href'),
                                price: $('a .good__price .good__price__new', this).html(),//TODO
                                imgLink: $('.good__img-img', this).data('original')
                            })
                        });
                        callback(null, items);
                    }
                })
            }

            //noinspection JSAnnotator
            function parseItemPage(item, callback) {
                request(uri + item.link, function (err, res, body) {
                    if (err) {
                        throw err;
                    } else {
                        var $ = cheerio.load(body, {decodeEntities: false});
                        var description = Array.prototype.reverse.call($('.product__description-text h3').eq(1).prevAll('p')).text();
                        var design = $('.product__description-text h3').eq(1).nextAll().text();
                        var howDoesItWork = $('.product__description-text-wide p').text();
                        var characteristic = $('.product__characteristics__tr').html();
                        var whatInBox = $('.product__narrow .product__prop-pist').html();
                        var isAvailable = $('.availability').html();

                        item['description'] = description;
                        item['design'] = design;
                        item['howDoesItWork'] = howDoesItWork;
                        item['characteristic'] = {};
                        $('.product__characteristics__tr').each(function (idx, elem) {
                            item['characteristic'][$(elem).children().first().text().trim()] = $(elem).children().last().text().trim();
                        });
                        item['whatInBox'] = [];
                        $('.product__prop-pist__item').each(function (idx, elem) {
                            item['whatInBox'].push($(elem).text().trim())
                        });
                        item['isAvailable'] = isAvailable;
                        callback(null, item);
                    }
                })
            }


            /*parseItemPage({
             id: 17,
             title: '3D-пазл UGears Бортовой грузовик (Flatbed truck)',
             category: 'Пазлы Ugears',
             link: '/p/mio-link-kardiomonitor/',
             price: null,
             imgLink: '/upload/resize_cache/iblock/3b4/213_159_0/3b4620fc5ecf444a7ba152c1dc906196.jpg'
             }, function (err, res) {
             if (err) {
             throw err;
             } else {
             console.log(res);
             }
             });*/

            //noinspection JSAnnotator
            function getPagerLength(href, categoryTitle, callback) {
                request(uri + href, function (err, res, body) {
                    if (err) {
                        throw err;
                    } else {
                        var $ = cheerio.load(body);

                        casper.start().then(function() {
                            this.open(uri + href, {
                                method: 'get',
                                headers: {
                                    'Accept': 'application/json'
                                }
                            });
                        });

                        casper.run(function() {
                            require('utils').dump(JSON.parse(this.getPageContent()));
                            this.exit();
                        });

                        var paginationLength = parseInt($('.pagination .pagination__item:last-child').text().trim(), 10);
                        var pSubject = {
                            title: categoryTitle,
                            link: href,
                            pLength: paginationLength
                        };
                        callback(null, pSubject);
                    }
                })
            }

            getPagerLength('/p/naushniki/','naushniki', function(err,res){
                if(err){
                    throw err;
                } else {
                    console.log(res);
                }
            })

            //noinspection JSAnnotator
            function setXlsx(items) {
                const Excel = require('exceljs');
                var workbook = new Excel.Workbook();
                const workbookProperties = {
                    creator: 'Evgeniy',
                    created: new Date(2016, 10, 5),
                    modified: new Date()
                };
                var filePath = './items.xlsx';


                workbook.views = [{
                    x: 0, y: 0, width: 10000, height: 20000,
                    firstSheet: 0, activeTab: 1, visibility: 'visible'
                }];
                var worksheet = workbook.addWorksheet("Products", {
                    pageSetup: {
                        paperSize: 9,
                        orientation: 'landscape',
                        margins: {
                            left: 0.7, right: 0.7,
                            top: 0.75, bottom: 0.75,
                            header: 0.3, footer: 0.3
                        },
                        fitToPage: true,
                        fitToHeight: 5,
                        fitToWidth: 7,
                        printArea: 'A1:G20'
                    },
                    properties: {
                        tabColor: {
                            argb: 'FFC0000'
                        },
                        showGridLines: false
                    },
                    views: [{
                        state: 'frozen',
                        xSplit: 1,
                        ySplit: 1,
                        activeCell: 'A1'
                    }]
                });

                worksheet.columns = [
                    {
                        header: 'Product_id',
                        key: 'id',
                        width: 10,
                        style: {
                            font: {
                                name: ''
                            }
                        }
                    },
                    {
                        header: 'Name(ru)',
                        key: 'title',
                        width: 52
                    },
                    {
                        header: 'Categories',
                        key: 'category',
                        width: 30
                    },
                    {
                        header: 'Description',
                        key: 'description',
                        width: 60
                    },
                    {
                        header: 'Design',
                        key: 'design',
                        width: 60
                    },
                    {
                        header: 'How does it work',
                        key: 'howDoesItWork',
                        width: 40
                    },
                    {
                        header: 'Characteristics',
                        key: 'characteristic',
                        width: 10
                    },
                    {

                        header: 'What in the box',
                        key: 'whatInBox',
                        width: 10
                    },
                    {
                        header: 'Is Available',
                        key: 'isAvailable',
                        width: 10
                    },
                    {
                        header: 'Price',
                        key: 'price',
                        width: 10
                    },
                    {
                        header: 'Image link',
                        key: 'imgLink',
                        width: 10
                    },
                ];

                worksheet.addRows(items);
                workbook.xlsx.writeFile(filePath)
                    .then(function () {
                        workbook.creator = workbookProperties.creator;
                        workbook.created = workbookProperties.created;
                        workbook.modified = workbookProperties.modified;
                    }, function (err) {
                        throw err;
                    });
            }


            /*async.map(itemList, function (i, callback) {
                getPagerLength(i.sectionLink, i.sectionTitle, function (err, item) {
                    if (err) {
                        throw err;
                    } else {
                        callback(null, item);
                    }
                })
            }, function (err, res) {
                if (err) {
                    throw err;
                } else {
                    var finallyItems = [];
                    async.eachLimit(/!*[res[0]]*!/res, 50, function (i, callback) {
                        async.times(i.pLength, function (n, next) {
                            parsePage(i.link, n, i.title, function (err, item) {
                                if (err) {
                                    throw err;
                                } else {
                                    next(err, item);
                                }
                            });
                        }, function (err, result) {
                            if (err) {
                                throw err;
                            } else {
                                finallyItems.push(result);
                                callback();
                            }
                        });
                    }, function () {
                        var _fI = [].concat.apply([], finallyItems);
                        var __fI = [].concat.apply([], _fI);
                        var ___fI = [].concat.apply([], __fI);
                        async.map(___fI, function (item, callback) {
                            parseItemPage(item, function (err, data) {
                                if (err) {
                                    throw err;
                                } else {
                                    callback(null, data);
                                }
                            });
                        }, function (err, response) {
                            if (err) {
                                throw err;
                            } else {
                                // console.log(response);
                                console.log(response);
                                // setXlsx(response);
                            }
                        });
                    });
                }
            });//end async.map*/


        }
    });

}();


