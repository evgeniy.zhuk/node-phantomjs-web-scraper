#Node-phantomjs-scraper 
##This is the simple node-phantomjs<br/> scraper based on node-phantom-async module.

![phantom-js][logo]

[logo]: https://chocolatey.org/content/packageimages/PhantomJS.2.1.1.png "phantomjs logo"

You should just require
```javascript
    import * as phantom from "node-phantom-async"
```
