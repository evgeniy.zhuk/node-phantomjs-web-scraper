"use strict";

const zipFolder = require('zip-folder'),
    request = require("requestretry"),
    cheerio = require("cheerio"),
    async = require("async"),
    log = require("cllc")(),
    net = require("net"),
    fs = require('fs');


net.createServer(function (socket) {
    socket.on("error", function (err) {
        if (err.code != "ECONNRESET") {
            log("Caught flash policy server socket error: ");
            log.error(err);
        }
    });
    socket.write("<?xml version=\"1.0\"?>\n");
    socket.write("<!DOCTYPE cross-domain-policy SYSTEM \"http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd\">\n");
    socket.write("<cross-domain-policy>\n");
    socket.write("<allow-access-from domain=\"*\" to-ports=\"*\"/>\n");
    socket.write("</cross-domain-policy>\n");
    socket.end();
}).listen(843);

const params = {
    method: "GET",
    uri: "http://madrobots.ru",
    bxrand: "1479661106462",
    bxajaxid: "4caf624afca571c030e8cfe3d2846c89",
    headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64)' +
        ' AppleWebKit/537.36 (KHTML, like Gecko)' +
        ' Chrome/27.0.1453.110 Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded',
        "cache-control": "no-cache"
    },
    imgBaseDir: './images',
    MAX_OPEN_FILES: 128,
    maxAttempts: 8,
    retryDelay: 5000,
    retryStrategy: request.RetryStrategies.HTTPOrNetworkError
};


function getCatalogLinks() {
    return function (callback) {
        const options = {
            method: params.method,
            url: params.uri,
            qs: {
                bxrand: params.bxrand
            },
            headers: params.headers,
            maxAttempts: params.maxAttempts,
            retryDelay: params.retryDelay,
            retryStrategy: params.retryStrategy
        };
        request(options, function (err, res, body) {
            if (err || res.statusCode !== 200) {
                return callback(err);
            } else {
                var $ = cheerio.load(body, {decodeEntities: false});
                var $catalogLinksList = $('.header .category .category__item');
                var catalogItems = [];
                log('\x1b[36m>\x1b[0m', "Start parsing catalog pages...");

                $catalogLinksList.each(function () {
                    catalogItems.push({
                        sectionLink: $("a.category__link", this).attr("href"),
                        sectionTitle: $(".category__title", this).text()
                    });
                });
                log(catalogItems.length, 'links parsed');
                callback(null, catalogItems);
            }
        })
    }
}

function getPaginatorLength(url, callback) {
    const options = {
        method: params.method,
        url: params.uri + url.sectionLink,
        qs: {
            bxajaxid: params.bxajaxid
        },
        headers: params.headers,
        maxAttempts: params.maxAttempts,
        retryDelay: params.retryDelay,
        retryStrategy: params.retryStrategy
    };
    request(options, function (err, res, body) {
        if (err || res.statusCode !== 200) {
            console.error(err);
        } else {
            var $ = cheerio.load(body, {decodeEntities: false});
            log(' processing ', options.url);
            url.paginationLength = parseInt($('.pagination a:last-child').text().trim(), 10);
            callback(null, url);
        }
    })
}

function parsePage(sectionLink, paginatorPage, category, callback) {
    const options = {
        method: params.method,
        url: params.uri + sectionLink,
        qs: {
            PAGEN_1: paginatorPage,
            bxajaxid: params.bxajaxid
        },
        headers: params.headers,
        maxAttempts: params.maxAttempts,
        retryDelay: params.retryDelay,
        retryStrategy: params.retryStrategy
    };

    var _writeQueue = async.queue(function (task, cb) {
        task(cb);
    }, params.MAX_OPEN_FILES);

    _writeQueue.drain = function () {
        if (_writeQueue.length() == 0) {
            log('Images have been processed');
        }
    };

    request(options, function (err, res, body) {
        if (err || res.statusCode !== 200) {
            console.error(err);
        } else {
            var $ = cheerio.load(body, {decodeEntities: false});
            log('processing', options.url + '?PAGEN_1=' + paginatorPage);
            var dirPath = params.imgBaseDir + '/' + category;
            if (!fs.existsSync(dirPath)) {
                fs.mkdirSync(dirPath)
            }
            var items = [];

            $(".catalog-grid__item").each(function (inc) {
                var itemTitle = $(".good__name", this).text();
                log('Founded', itemTitle);
                var imgPath = $(".good__img-img", this).data("original");

                const subOptions = {
                    method: params.method,
                    url: params.uri + imgPath,
                    headers: params.headers,
                    maxAttempts: params.maxAttempts,
                    retryDelay: params.retryDelay,
                    retryStrategy: params.retryStrategy
                };
                items.push({
                    id: inc,
                    title: itemTitle,
                    category: category,
                    link: $("a", this).attr("href"),
                    imgLink: imgPath,
                    price: $('.good__price__new', this).clone().children().remove().end().text().trim().replace(' ', '').replace('&nbsp;', '').length === 0 ? '-' : $('.good__price__new', this).clone().children().remove().end().text().trim().replace(' ', '').replace(/&nbsp;/g, '')
                });

                _writeQueue.push(function (cbck) {
                    request(subOptions)
                        .on('error', function (err) {
                            log.error(itemTitle, '->', err);
                        })
                        .pipe(fs.createWriteStream(dirPath + '/' + imgPath.match(/([^\/]+)$/)[0]))
                        .on('finish', function () {
                            log('Finish processing', imgPath.match(/([^\/]+)$/)[0])
                        });
                    cbck();
                }, function (err) {
                    if (err) {
                        log.error(err);
                    }
                });

            });
            callback(null, items);
        }
    })
}


function parseItemPage(item, callback) {
    const options = {
        method: params.method,
        url: params.uri + item.link,
        headers: params.headers,
        maxAttempts: params.maxAttempts,
        retryDelay: params.retryDelay,
        retryStrategy: params.retryStrategy
    };
    request(options, function (err, res, body) {
        if (err || res.statusCode !== 200) {
            console.error(err)
        } else {
            log('scraping', item.title);
            var $ = cheerio.load(body, {decodeEntities: false});

            Object.defineProperties(item, {
                price: {
                    value: $('meta[itemprop = "price"]').length === 0 ? $('.product__purchase-price').clone().children().remove().end().text().trim() : $('meta[itemprop = "price"]').attr("content") != 0 ? $('meta[itemprop = "price"]').attr("content") : "-",
                    configurable: true,
                    writable: true,
                    enumerable: true
                },
                description: {
                    value: $(".product__description").children().first().is("h3") ? $(".product__description-text-wide h3").eq(0).nextUntil(".product__description-text-wide h3").eq(1).text().trim() : Array.prototype.reverse.call($(".product__description-text h3").eq(1).prevAll("p")).text().trim().replace(/&nbsp;/g, " "),
                    configurable: true,
                    writable: true,
                    enumerable: true
                },
                design: {
                    value: $(".product__description h3").eq(2).next().is("p") ? $(".product__description h3").eq(2).nextUntil(".product__description h3:nth-of-type(3)").text().trim().replace(/&nbsp;/g, " ") : $(".product__description h3").eq(2).parent().clone().children().each(function (i, elem) {
                        $(elem).remove()
                    }).end().text().trim(),
                    configurable: true,
                    writable: true,
                    enumerable: true
                },
                howDoesItWork: {
                    value: $(".product__description-text-wide").children().first().is("h3") ? $(".product__description-text-wide").clone().find("h3").remove().end().text().trim() : $(".product__description-text-wide h3:nth-of-type(3)").nextUntil(".product__description-text-wide h3:nth-of-type(4)").text().trim(),
                    configurable: true,
                    writable: true,
                    enumerable: true
                },
                characteristics: {
                    value: [],
                    configurable: true,
                    writable: true,
                    enumerable: true,
                    sortable: true,
                    resizeable: true
                },
                whatIsInBox: {
                    value: [],
                    configurable: true,
                    writable: true,
                    enumerable: true
                },
                isAvailable: {
                    value: item.price === '-' ? 'no' : 'yes' /*$('meta[itemprop="availability"]').length === 0 ? $('.availability').length === 0 ? "Can not be determined" : $('.availability').clone().removeClass('availability').end().attr('class').split('-')[1] : $('meta[itemprop="availability"]').attr("content").match(/([^\/]+)$/)[0] === "InStock" ? "yes" : "no"*/,
                    configurable: true,
                    writable: true,
                    enumerable: true
                }
            });

            $(".product__characteristics__tr").each(function (idx, elem) {
                item.characteristics[$(elem).children().first().text().trim()] = $(elem).children().last().text().trim();
            });
            $(".product__prop-pist__item").each(function (idx, elem) {
                item.whatIsInBox.push($(elem).text().trim())
            });

            callback(null, item);
        }
    })
}


function setXlsx(items) {
    const Excel = require('exceljs');
    var workbook = new Excel.Workbook();
    const workbookProperties = {
        creator: 'Evgeniy',
        created: new Date(2016, 10, 5),
        modified: new Date()
    };
    var filePath = './items.xlsx';

    workbook.views = [{
        x: 0, y: 0, width: 10000, height: 20000,
        firstSheet: 0, activeTab: 1, visibility: 'visible'
    }];
    var worksheet = workbook.addWorksheet("Products", {
        pageSetup: {
            paperSize: 9,
            orientation: 'landscape',
            margins: {
                left: 0.7, right: 0.7,
                top: 0.75, bottom: 0.75,
                header: 0.3, footer: 0.3
            },
            fitToPage: true,
            fitToHeight: 5,
            fitToWidth: 7,
            printArea: 'A1:G20'
        },
        properties: {
            tabColor: {
                argb: 'FFC0000'
            },
            showGridLines: false
        },
        views: [{
            state: 'frozen',
            xSplit: 1,
            ySplit: 1,
            activeCell: 'A1'
        }]
    });

    worksheet.columns = [{
        header: 'Product_id',
        key: 'id',
        width: 10,
        style: {
            font: {
                name: ''
            }
        }
    }, {
        header: 'Name(ru)',
        key: 'title',
        width: 52
    }, {
        header: 'Categories',
        key: 'category',
        width: 30
    }, {
        header: 'Description',
        key: 'description',
        width: 60
    }, {
        header: 'Design',
        key: 'design',
        width: 60
    }, {
        header: 'How does it work',
        key: 'howDoesItWork',
        width: 40
    }, {
        header: 'Characteristics',
        key: 'characteristic',
        width: 10
    }, {
        header: 'What in the box',
        key: 'whatInBox',
        width: 10
    }, {
        header: 'Is Available',
        key: 'isAvailable',
        width: 10
    }, {
        header: 'Price',
        key: 'price',
        width: 10
    }, {
        header: 'Image link',
        key: 'imgLink',
        width: 10
    }];
    log('\x1b[36m>\x1b[0m', "Start writing xlsx...");

    var start = new Date().getTime();

    worksheet.addRows(items);

    workbook.xlsx.writeFile(filePath)
        .then(function () {
            workbook.creator = workbookProperties.creator;
            workbook.created = workbookProperties.created;
            workbook.modified = workbookProperties.modified;
        }, function (err) {
            throw err;
        });
    log('\x1b[36m>\x1b[0m', "Finished in " + (new Date().getTime() - start + 'ms.'));
}


async.waterfall([
    getCatalogLinks(),
    function (catalogLinks, cb) {
        log('\x1b[36m>\x1b[0m', "Start getting paginators length...");
        async.map(catalogLinks, function (catalogLink, next) {
            getPaginatorLength(catalogLink, function (err, url) {
                if (err) {
                    cb(err);
                } else {
                    next(null, url);
                }
            })
        }, function (err, res) {
            if (err) {
                cb(err);
            } else {
                log('Finish getting pagination length');
                cb(null, res);
            }
        })
    },
    function (items, cb) {
        log('\x1b[36m>\x1b[0m', "Start parsing pages...");
        const concurrence = 5;
        var finallyItems = [];
        if (!fs.existsSync(params.imgBaseDir)) {
            fs.mkdirSync(params.imgBaseDir)
        }
        async.each(items, /*concurrence,*/ function (i, callback) {
            log(JSON.stringify(i));
            async.times(i.paginationLength, function (n, next) {
                parsePage(i.sectionLink, n + 1, i.sectionTitle, function (err, item) {
                    if (err) {
                        cb(err);
                    } else {
                        next(null, item);
                    }
                })
            }, function (err, res) {
                if (err) {
                    cb(err);
                } else {
                    finallyItems.push(res);
                    callback()
                }
            })
        }, function () {
            cb(null, [].concat.apply([], [].concat.apply([], [].concat.apply([], finallyItems))));
        })
    },
    function (itemList, cb) {
        log('\x1b[36m>\x1b[0m', "Start parsing items...");
        const limit = 50;
        async.mapLimit(itemList, limit, function (item, next) {
            parseItemPage(item, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    next(null, data);
                }
            })
        }, function (err, response) {
            if (err) {
                cb(err);
            } else {
                log('Finish parsing items.');
                cb(null, response);
            }
        })
    }
], function (err, res) {
    if (err) {
        throw err;
    } else {
        setXlsx(res);
        zipFolder(params.imgBaseDir, params.imgBaseDir + '.zip', function (err) {
            if (err) {
                log.error('Error while writing zip file', err);
            } else {
                log('Folder with pages has been zipped,');
            }
        });
    }
});


