"use strict";
const phridge = require('phridge');
const async = require('async');
const when = require('when');

const uri = "http://www.madrobots.ru";

const phridgeConfig = {
    //"--debug": "true",
    "autoLoadImages": 'no',
    "ignoreSslErrors": 'yes',
    "outputEncoding": 'utf8',
    "sslProtocol": 'any',
    "webSecurityEnabled": false
};

function parseCatalogLinks(callback) {
    var page = null;
    phridge.spawn(phridgeConfig)
        .then(function (phantom) {
            return phantom.openPage(uri);
        })
        .catch(function (err) {
            console.error('Can\'t open the page', uri, err);
        })
        .then(function (pageInstance) {
            page = pageInstance;
            page.settings = {
                userAgent: "Mozilla/5.0 (Windows NT 6.3; WOW64)" +
                " AppleWebKit/537.36 (KHTML, like Gecko)" +
                " Chrome/54.0.2840.87 Safari/537.36",
                loadImages: false
            };

            return page.run(function () {
                var page = this;
                return page.evaluate(function () {
                    var $catalogLinksList = $('.header .category .category__item');
                    var catalogItems = [];

                    $catalogLinksList.each(function () {
                        catalogItems.push({
                            sectionLink: $('a.category__link', this).attr('href'),
                            sectionTitle: $('.category__title', this).text()
                        });
                    });

                    return catalogItems;
                });
            });
        })
        .then(function (result) {
            callback(null, result);
        })
        .then(function () {
            page.dispose().then(function () {
                console.log('page %s has been terminated', uri)
            })
        })
        .catch(function (err) {
            console.error(err.stack);
        })
        .then(phridge.exit)
}


function getPaginatorLength(urls, callback) {
    var pages = [];
    phridge.spawn(phridgeConfig)
        .then(function (phantom) {
            return when.map(urls, function (url) {
                return phantom.openPage("http://www.madrobots.ru" + url.sectionLink);
            });
        })
        .then(function (pageInstances) {
            pages = pageInstances;
            return when.map(pages, function (page) {
                page.settings = {
                    userAgent: "Mozilla/5.0 (Windows NT 6.3; WOW64)" +
                    " AppleWebKit/537.36 (KHTML, like Gecko)" +
                    " Chrome/54.0.2840.87 Safari/537.36",
                    loadImages: false
                };
                return page.run(function () {
                    //var page = this;
                    return this.evaluate(function () {
                        return {
                            pLength: parseInt($('.pagination a:last-child').text().trim(), 10)
                        }
                    });
                });

            });
        })
        .then(function (results) {
            var res = results;
            urls.map(function (url) {
                url.paginationLength = res.shift().pLength
            });
            return urls;
        })
        .then(function () {
            callback(null, urls);
        })
        .catch(function (err) {
            console.error(err.stack);
        })
        .then(function () {
            when.map(pages, function (page) {
                page.dispose();
            });
            console.log('All pages have been closed');
        })
        .then(phridge.disposeAll)
}

async.waterfall([
    function (cb) {
        parseCatalogLinks(function (err, res) {
            if (err) {
                cb(err, null)
            } else {
                cb(null, res);
            }
        })
    },
    function (catalogLinks, cb) {
        getPaginatorLength(catalogLinks/*.slice(0, 10)*/, function (err, res) {
            if (err) {
                cb(err, null);
            } else {
                cb(null, res);//TODO: res
            }
        })
    }
], function (err, res) {
    if (err) {
        throw err;
    } else {
        console.log(res);
    }
});
