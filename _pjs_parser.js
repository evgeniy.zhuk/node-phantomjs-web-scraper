'use strict';
const async = require('async');
const phantom = require('node-phantom-async');
process.setMaxListeners(Infinity);

const uri = 'http://www.madrobots.ru';

const phantomCustomPageSettings = {
    XSSAuditingEnabled: false,
    javascriptCanCloseWindows: true,
    javascriptCanOpenWindows: true,
    javascriptEnabled: true,
    loadImages: false,
    localToRemoteUrlAccessEnabled: false,
    userAgent: 'Mozilla/5.0 (Windows NT 6.3; WOW64)' +
    ' AppleWebKit/537.36 (KHTML, like Gecko)' +
    ' Chrome/54.0.2840.87 Safari/537.36',
    webSecurityEnabled: true
};

function parseCatalogLinks(callback) {
    phantom.create({
        phantomPath: require('phantomjs').path,
        ignoreSSLErrors: true
    })
        .bind({})
        .then(function (ph) {
            this.ph = ph;
            return this.ph.createPage();
        })
        .catch(function (err) {
            console.error('Error while creating the page :', err);
        })
        .then(function (page) {
            this.page = page;
            this.page.set('settings', phantomCustomPageSettings, function (err) {
                if (err) {
                    console.error('Error while applying settings ', err);
                } else {
                    // console.log('Setting has been applied');
                }
            });
            return this.page;
        })
        .then(function (page) {
            // objectPath.set(this.page.settings)
            return this.page.open(uri);
        })
        .catch(function (err) {
            console.error('Error while opening main page', err);
        })
        .then(function (status) {
            console.info('Main site ' + uri + ' opened with status', status);
        })
        .then(function () {
            return this.page.evaluate(function () {
                var $catalogLinksList = $('.header .category .category__item');
                var catalogItems = [];

                $catalogLinksList.each(function () {
                    catalogItems.push({
                        sectionLink: $('a.category__link', this).attr('href'),
                        sectionTitle: $('.category__title', this).text()
                    });
                });
                return catalogItems;
            });
        })
        .catch(function (err) {
            console.error('Error while evaluating the main page', uri, err);
        })
        .then(function (result) {
            callback(null, result);
        })
        .finally(function () {
            this.ph.exit();
        });
}

function _getPagerLength(url, callback) {
    phantom.create({
        phantomPath: require('phantomjs').path,
        ignoreSSLErrors: true,
        maxTryFinishRes: 3,
        isDebug: 1
    })
        .bind({})
        .then(function (ph) {
            this.ph = ph;
            this.href = url.sectionLink;
            return this.ph.createPage();
        })
        .catch(function () {
            console.error('Error while creating the page', uri + this.href);
        })
        .then(function (page) {
            this.page = page;
            this.page.set('settings', phantomCustomPageSettings, function (err) {
                if (err) {
                    console.error('Error while applying settings ', err);
                } else {
                    // console.log('Setting has been applied');
                }
            });
            return this.page
        })
        .then(function (page) {
            page.open(uri + this.href);
        })
        .catch(function (err) {
            console.error('error while opening the page:', uri + this.href, err);
        })
        // .delay(800)
        .then(function (status) {
            console.log('site', (uri + this.href), 'opened with status', status);
            return this.page.get('content');
        })
        .catch(function (err) {
            console.error('Error while getting the content', err);
        })
        .then(function (content) {
            // console.log('content', content)
            var self = this;
            return this.page.evaluate(function () {
                // var t =document.getElementsByClassName("pagination")[0].children[t.children.length-1].getAttribute("href").split("=")[1]
                console.log('evaluating');
                return {
                    pLength: parseInt($('.pagination a:last-child').text().trim(), 10)
                };
            });
        })
        .catch(function (err) {
            console.error('Error while evaluating the page', uri + this.href, ':', err);
        })
        .then(function (res) {

            this.page.onLoadFinished = function (status) {
                console.log('onLoadFinished with status', status);
            };

            console.log(res);
            try {
                url.paginationLength = res.pLength;
                callback(null, url);
            } catch (e) {
                console.error('Error while defining property', e.stack);
            }
        })
        .finally(function () {
            this.ph.exit();
        });
}

async.waterfall([
    function (cb) {
        parseCatalogLinks(function (err, res) {
            if (err) {
                cb(err, null);
            } else {
                cb(null, res);
            }
        });
    },
    function (catalogLinks, cb) {
        async.mapSeries(catalogLinks, function (i, next) {
            _getPagerLength(i, function (err, page) {
                if (err) {
                    next(err);
                } else {
                    next(null, page);
                }
            });
        }, function (err, res) {
            if (err) {
                cb(err);
            } else {
                cb(null, res);
            }
        });
    },
    function (catalogItems, callback) {
        console.log(catalogItems);
        callback(null, null);
    }
], function (err, res) {
    if (err) {
        throw err;
    } else {
        console.log(res);
    }
});
